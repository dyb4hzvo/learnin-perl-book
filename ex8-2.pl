#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

open FLINSTONES, "<", "flinstones.txt";

while(<FLINSTONES>) {
  chomp;
  if(/\b\w*a\b/) {
    print "Matched: |$`<$&>$'|\n";
  } else {
    print "No match: |$_|\n";
  }
}
