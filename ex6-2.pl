#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

say "Enter words, one per line.";
chomp(my @words = <STDIN>);

my %words_count;

foreach my $word (@words) {
  if(exists $words_count{$word}) {
    $words_count{$word} += 1;
  } else {
    $words_count{$word} = 1;
  }
}

foreach my $word (sort keys %words_count) {
  printf "%-20s %s\n", $word, $words_count{$word};
}
