#!/usr/bin/env perl

print "Input string\n";
chomp($string = <STDIN>);
print "Input repeat count\n";
$repeat_count = <STDIN>;
$i = 0;
while($i < $repeat_count) {
  print $string . "\n";
  $i++;
}
