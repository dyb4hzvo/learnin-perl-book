#!/usr/bin/perl

use 5.010;
use strict;

sub greet {
  state @prev_names = ();
  my $curr_name = $_[0];
  print "Hi $curr_name! ";
  if(@prev_names == 0) {
    print "You are the first one here!";     
  } else {
    print "I've seen: @prev_names";
  }
  push(@prev_names,  $curr_name);
  print "\n";
}

greet( "Fred" );
greet( "Barney" );
greet( "Wilma" );
greet( "Betty" );
