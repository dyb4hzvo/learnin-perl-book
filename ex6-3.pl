#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

say "VENV wariables:\n";

foreach my $key (sort keys %ENV) {
  printf("%-30s %s\n", $key, $ENV{$key});
}
