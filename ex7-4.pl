#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

open FLINSTONES, "<", "flinstones.txt";
chomp(my @flinstones = <FLINSTONES>);
foreach (@flinstones) {
    if(/([a-z]+[A-Z]+)|([A-Z]+[a-z]+)/) {
        say $_;
    }
}
