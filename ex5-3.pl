#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

my $width = <STDIN>;

say "Set output width $width";

my @input = <STDIN>;

for(my $i = 1; $i <= ($width % 10 + 1) * 10; $i++) {
  print $i % 10;
}
print "\n";



foreach my $string (@input) {
  my $fmt = "%" . ($width + 1) . "s\n";
  printf $fmt, $string;
}
