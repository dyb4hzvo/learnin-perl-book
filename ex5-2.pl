#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

my @input = <STDIN>;

say "1234567890123456789012345678901234567890";
foreach my $string (@input) {
  printf "%21s", $string;
}
