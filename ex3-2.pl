#!/usr/bin/perl

use 5.010;

@names = qw(fred betty barney dino wilma pebbles bamm-bamm);

say "Input numbers";
@numbers = <STDIN>;

foreach $number (@numbers) {
  say $names[$number - 1];
}


