#!/usr/bin/perl

use 5.010;
use strict;

sub greet {
  state $prev_name = undef;
  my $curr_name = $_[0];
  print "Hi $curr_name! ";
  if(defined $prev_name ) {
      print "$prev_name is also here!";
  } else {
      print "You are the first one here!";   

  }
  $prev_name = $curr_name;
  print "\n";
}

&greet( "Fred" );
&greet( "Barney" );
