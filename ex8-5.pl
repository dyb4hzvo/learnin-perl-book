#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

open FLINSTONES, "<", "flinstones.txt";

while(<FLINSTONES>) {
  chomp;
  if(/(?<word>\b\w*a\b)(?<next>.{,5})/) {
    print "Matched: |$`<$&>$'|\n";
    foreach my $key (keys %+) {
      print "'$key' contains '$+{$key}'\n";
    }
  } else {
    print "No match: |$_|\n";
  }
}
