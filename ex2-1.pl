#!/usr/bin/env perl

$radius = 12.5;
$pi = 3.141592654;
$circle_length = 2 * $pi * $radius;

print $circle_length . "\n";
