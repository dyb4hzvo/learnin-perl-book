#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

my %peoples = (
  "fred" => "flinstone",
  "barney" => "rubble",
  "wilma" => "flinstone",
  "semen" => "persunov"
);

say "Please enter name";
chomp(my $name = <STDIN>);

if(exists $peoples{$name}) {
  say "Surname of $name is $peoples{$name}";
} else {
  say "I don't know surname of $name";
}
