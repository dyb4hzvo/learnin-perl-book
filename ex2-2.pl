#!/usr/bin/env perl

print "Please, enter radius\n";
$radius = <STDIN>;
$pi = 3.141592654;
$circle_length = 2 * $pi * $radius ;

print 'Circle length is ' . $circle_length . "\n";
