#!/usr/bin/perl

use 5.010;

chomp(@strings = <STDIN>);
@strings = sort(@strings);

foreach $string (@strings) {
  say $string;
}

