#!/usr/bin/perl

use 5.010;
use strict;

sub total {
  my $sum = 0;
  foreach my $num (@_) {
    $sum += $num;
  }
  $sum;
}

my $sum = &total(1..1000);

print "Sum from 1 to 1000 is $sum \n";
