#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

open FLINSTONES, "<", "flinstones.txt";
chomp(my @flinstones = <FLINSTONES>);
foreach (@flinstones) {
    if(/[Ff]red/) {
        say $_;
    }
}
