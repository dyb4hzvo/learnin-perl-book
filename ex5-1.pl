#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

foreach my $file_name (reverse @ARGV) {
  if(! open INPUT_FILE, $file_name) {
    die "Cannot to open file $file_name: $!";
  }
  my @lines = <INPUT_FILE>;
  foreach my $line (reverse @lines) {
    chomp $line;
    say $line;
  }
}
